# cpustats

Emodule cpu stat fork in order to have something like Android cpustats app. I think that the original enlightenment cpu stat is not sensible at all.

I wanted sthg that look like Android cpustats but I have no time to learn evas&co so I implemented a simple cpu bar using numeric, it is far less eyes candy, but as sensible to check quickly cpu usage :

```
// algo
foreach cpu:
  if (cpuUsage < 5) {
    return "";
  } else if (cpuUsage < 10) {
    return "1";
  } else if (cpuUsage < 20) {
    return "12";
  } else if (cpuUsage < 40) {
    return "124";
  } else if (cpuUsage < 60) {
    return "1246";
  } else if (cpuUsage < 80) {
    return "12468";
  } else {
    return "12468X";
  }
```

4 cpu screenshot (1st cpu is at ~100%, 2nd at 60%, 3rd at ~20%, 4th at ~40%)

![4 cpus](images/screenshot4.png)

1 cpu screenshot (1st or averall cpu is at ~40%)

![1 cpu](images/screenshot1.png)

Works from 1 to 4 cpu. 

I did not test it with more than 4 cpu, should be weird has there will be not enough space for the font.
Perhaps it can work by lowering the font\_size param in [cpu.edc](cpu.edc) file.

